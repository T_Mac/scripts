import requests
import json
import threading
import logging
moduleLogger = logging.getLogger('Reptile.Consumer')
CHUNK_SIZE = 4096



def callbackStub(message):
	pass

def callbackPrint(message):
	print str(message)

class Consumer(threading.Thread):
	def __init__(self, target, callback = callbackStub, strictChecking = False):
		super(Consumer, self).__init__()
		self.daemon = True

		self.target = target
		self.callback = callback
		self.StrictChecking = strictChecking

		self.Monitor = self.start

		self.incompletePacket = ''
		self.__Prequel = False
		self.__Ending = False

	def run(self):
		response = self.__watchQueue(self.target)
		for chunk in response.iter_content():
			#moduleLogger.debug('got chunk %s'%str(chunk))
			message = self.__processChunk(chunk)
			if message:
				self.callback(message)

	def __watchQueue(self, addr):
		moduleLogger.debug('watching %s'%addr)
		response = requests.get(addr, stream=True) #blocks here
		moduleLogger.debug('wathQueue returned')
		return response

	def __detectMessageEnd(self,chunk):
		if chunk == '\r':
			moduleLogger.debug('got PreEnding')
			self.__Prequel = True

		if chunk == '\n':
			moduleLogger.debug('got Ending')
			self.__Ending = True

		if self.__Ending:
			if self.__Prequel:
				moduleLogger.debug('Message ending detected')
				self.__resetMessageState()
				return True

			else:
				if self.StrictMessageChecking:
					moduleLogger.error('Got premature newline ending, strict checking Enabled, not signaling message end')
					moduleLogger.error('Reseting Message State')
					self.__resetMessageState()
					return False

				else:
					moduleLogger.error('Got premature newline ending, strict checking Disabled, signaling message end')
					self.__resetMessageState()
					return True

		#moduleLogger.error("Error in message detection, shouldn't have gotten this far!")
		moduleLogger.debug('message end not detected')
		return False

	def __resetMessageState(self):
		self.__Prequel = False
		self.__Ending = False

	def __processChunk(self, chunk):
		moduleLogger.debug('processing chunk')
		if not self.__detectMessageEnd(chunk):
			self.incompletePacket = self.incompletePacket+chunk

		else:
			try:
				moduleLogger.debug('trying json load')
				data = json.loads(self.incompletePacket)

			except ValueError as e:
				return None

			else:
				moduleLogger.debug('load successful')
				self.incompletePacket = ''
				return data



def Watch(*args, **kwargs):
	con = Consumer(*args, **kwargs)
	con.Monitor()
	return con

if __name__ == '__main__':
	#setup logging for tests
	import sys
	loggingConfig = dict(
		stream = sys.stderr,	#Logfile
		filename = None,		# Use stream or filename but not both
		level = logging.DEBUG,                                        #Logging Level
		format = '%(asctime)s %(name)s %(levelname)s: %(message)s',   #Log message Format
		datefmt='%Y-%m-%d %H:%M:%S',
		)
	logging.basicConfig(**loggingConfig)
	addr = raw_input('queue address:')
	c = Watch(addr, callback=callbackPrint)
	raw_input()
