#!/usr/bin/env python3

import glob
import sys
from os.path import expanduser
from collections import defaultdict
import subprocess

#Usage findKeys.py [-pem] /dir /dir2 Where dir is a directory to search IN ADDITION to ~/.ssh

home = expanduser('~')
DEFAULT_PATH = home + '/.ssh'
VALID_OPTIONS = ['-pem', '-gpg']
VERSION = '0.4'
USAGE = '''findKeys ver %s Copyright Taylor McKinnon
Usage findKeys.py [-pem] [-gpg] [/dir] ... Where dir is a directory to search IN ADDITION to ~/.ssh
	-pem	Toggles inclusion of *.pem encoded keys
	-gpg	Toggles inclusion of gpg ids\n'''%VERSION

def parseFlags():
	args = sys.argv
	options = defaultdict(lambda: False)
	options['dirs'] = [DEFAULT_PATH]
	if len(args) > 1:
		for flag in args[1:]:
			if flag[0] == '-':
				if flag in VALID_OPTIONS:
					options[flag[1:]] = True
				else:
					print(USAGE)
					exit(1)
				
			else:
				if flag[-1:] == '/':
					flag = flag[:-1]
				options['dirs'].append(flag)
	return options

def collectSSHkeys(options):
	key_paths = []
		
	possiblePrivateKeys = collectPrivateKeys(options['dirs'])
	possiblePublicKeys = collectPublicKeys(options['dirs'])
	
	if options['pem']:
		for key in possiblePrivateKeys:
			if key[-4:] == '.pem':
				key_paths.append(key)

	for key in possiblePublicKeys:
		base_name = key.replace('.pub', '')
		if base_name in possiblePrivateKeys:
			key_paths.append(base_name)

	return key_paths

def collectPrivateKeys(dirs = []):
	possibleKeys = []
	for dir in dirs:
		possibleKeys = possibleKeys + glob.glob(dir + '/*')
	return possibleKeys

def collectPublicKeys(dirs = []):
	possibleKeys = []
	for dir in dirs:
		possibleKeys = possibleKeys + glob.glob(dir + '/*.pub')

	return possibleKeys

def collectGPGkeys():
	raw = subprocess.check_output("gpg -k | grep 'pub' | awk '{ print substr($2,9) }'", shell=True)
	return raw[1:-1].split('\n')

def renderOutput(keys, options):
	output = ''
	for key in privateKeys:
		output = output + key + ' '
	if options['gpg']:
		for id in collectGPGkeys():
			output = output + id + ' '
	return output.rstrip()
	
if __name__ == '__main__':
	options =  parseFlags()
	privateKeys = collectSSHkeys(options)
	print(renderOutput(privateKeys, options))


