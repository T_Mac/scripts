#!/bin/bash

if [ -z $1 ]; then
	BOX="/home/tmac/.locked"
else
	BOX="$1"
fi
LOOP=$(losetup -f)

sudo losetup $LOOP $BOX
sudo cryptsetup open $LOOP lockbox
sudo mount -o rw /dev/mapper/lockbox ~/.lockbox
set -x LOCKBOX ~/.lockbox
echo -e "\e[32m-Lockbox Loaded-"
echo -e "\e[39m"
