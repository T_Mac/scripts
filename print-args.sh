#!/bin/bash

printargs(){
	echo -n " "
}

if [ "$1" == "--print-args" ]
	then
	printargs
	exit
fi

exec $1 $2
